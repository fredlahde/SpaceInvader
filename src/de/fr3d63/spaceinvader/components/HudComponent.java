package de.fr3d63.spaceinvader.components;

import de.fr3d63.spaceinvader.config.Config;
import de.fr3d63.spaceinvader.controller.GameController;
import de.fr3d63.spaceinvader.player.Player;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.List;

public class HudComponent implements IComponent {

    private final Player player;
    private final GameController gameController;


    public HudComponent(Player player, GameController gameController) {
        this.player = player;
        this.gameController = gameController;
    }

    @Override
    public void update(double elapsedTime) {

    }

    @Override
    public void handleInput(List<String> inputs) {

    }

    @Override
    public void handleCollision() {

    }

    @Override
    public void render(GraphicsContext graphicsContext) {

        graphicsContext.strokeLine(0, Config.GAMEOVER_BOUND - 1, 1024, Config.GAMEOVER_BOUND);

        graphicsContext.setFill(Color.WHITE);
        graphicsContext.setStroke(Color.WHITE);

        if (gameController.isGameOver()) {
            graphicsContext.setFont(new Font(45));
            graphicsContext.fillText("Game over!\nScore: " + player.getScore() + "\nPress R to reset", 400, 400);
            return;
        }

        graphicsContext.setFont(new Font(25));
        String text = "Score: " + String.valueOf(player.getScore());
        final int posX = 900;
        final int posY = 960;
        graphicsContext.fillText(text, posX, posY);
        graphicsContext.fillText(String.format("%.0f FPS", gameController.getFps()), posX, posY + 30);


    }
}

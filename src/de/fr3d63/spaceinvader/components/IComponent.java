package de.fr3d63.spaceinvader.components;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.List;

public interface IComponent {

    void update(final double elapsedTime);

    void handleInput(final List<String> inputs);

    void handleCollision();

    void render(GraphicsContext graphicsContext);

    default void drawBoundaries(final GraphicsContext graphicsContext, final Rectangle2D boundary, final Color color) {
        graphicsContext.setStroke(color);
        graphicsContext.strokeRect
                (
                        boundary.getMinX(),
                        boundary.getMinY(),
                        boundary.getWidth(),
                        boundary.getHeight()
                );
        graphicsContext.setStroke(Color.WHITE);
    }
}

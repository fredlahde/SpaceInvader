package de.fr3d63.spaceinvader.components;

import de.fr3d63.spaceinvader.components.container.ComponentContainer;
import de.fr3d63.spaceinvader.controller.GameController;
import de.fr3d63.spaceinvader.player.Player;
import javafx.scene.canvas.GraphicsContext;

import java.util.List;

public class ResetComponent implements IComponent {

    private final GameController gameController;
    private final Player player;

    public ResetComponent(GameController gameController, Player player) {
        this.gameController = gameController;
        this.player = player;
    }

    @Override
    public void update(double elapsedTime) {

    }

    @Override
    public void handleInput(List<String> inputs) {
        if (gameController.isGameOver()) {
            if (inputs.contains("R")) {
                ((AsteroidComponent) ComponentContainer.getInstance().getComponent(AsteroidComponent.class.getSimpleName())).reset();
                gameController.setGameOver(false);
                player.setScore(0);
                player.setCanScore(true);
                inputs.remove("R");
            }
        }
    }

    @Override
    public void handleCollision() {

    }

    @Override
    public void render(GraphicsContext graphicsContext) {

    }
}

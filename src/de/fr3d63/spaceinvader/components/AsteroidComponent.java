package de.fr3d63.spaceinvader.components;

import de.fr3d63.spaceinvader.config.Config;
import de.fr3d63.spaceinvader.controller.GameController;
import de.fr3d63.spaceinvader.player.Player;
import de.fr3d63.spaceinvader.sprites.Asteroid;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AsteroidComponent implements IComponent {


    private final List<Asteroid> asteroidList;
    private final Image targetTexture;
    private final Random rand;
    private final GameController gameController;
    private final Player player;
    private boolean hasInitialized = false;

    public AsteroidComponent(GameController gameController, Player player) {
        this.gameController = gameController;
        this.player = player;
        this.targetTexture = new Image(Config.TARGET_IMAGE_PATH);

        this.asteroidList = new ArrayList<>();

        rand = new Random();
        createAsteroids(25);
    }

    public List<Asteroid> getAsteroidList() {
        return asteroidList;
    }

    @Override
    public void update(double elapsedTime) {

        if (!gameController.isGameOver()) {
            if (hasInitialized) {
                asteroidList.forEach(asteroid -> asteroid.addVelocity(0, .06f));
            } else if (!hasInitialized && elapsedTime / 1000 > 12) {
                hasInitialized = true;
            }
        } else {
            asteroidList.forEach(asteroid -> asteroid.setVelocity(0, 0));
        }

        asteroidList.forEach(asteroid -> asteroid.update(elapsedTime));
    }

    @Override
    public void handleInput(List<String> inputs) {

    }

    @Override
    public void handleCollision() {
        if (hasInitialized) {
            asteroidList.forEach(asteroid -> {
                if (asteroid.getPosition().y + asteroid.getSize().y > Config.GAMEOVER_BOUND) {
                    gameController.setGameOver(true);
                    player.setCanScore(false);
                }

            });
        }

    }

    @Override
    public void render(GraphicsContext graphicsContext) {
        asteroidList.forEach(asteroid -> {
            asteroid.render(graphicsContext);
            if (Config.DRAW_SPRITE_BOUNDARY) {
                drawBoundaries(graphicsContext, asteroid.getBoundary(), Color.BLUE);
            }
        });

    }

    void createAsteroids(final int count) {

        for (int i = 0; i < count; i++) {
            int x = rand.nextInt(Config.TARGET_MAX_X - Config.TARGET_MIN_X) + Config.TARGET_MIN_X;
            int y = rand.nextInt(Config.TARGET_MAX_Y) + 1;

            Asteroid asteroid = new Asteroid(targetTexture);
            asteroid.setSize(50, 50);
            asteroid.setPosition(x, y);
            this.asteroidList.add(asteroid);
        }
    }

    public void reset() {
        this.asteroidList.clear();
        this.createAsteroids(25);
    }
}

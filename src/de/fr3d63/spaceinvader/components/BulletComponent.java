package de.fr3d63.spaceinvader.components;

import de.fr3d63.spaceinvader.components.container.ComponentContainer;
import de.fr3d63.spaceinvader.config.Config;
import de.fr3d63.spaceinvader.controller.GameController;
import de.fr3d63.spaceinvader.player.Player;
import de.fr3d63.spaceinvader.sprites.Bullet;
import de.fr3d63.spaceinvader.sprites.Ship;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class BulletComponent implements IComponent {

    private final List<Bullet> bulletList;
    private final Player player;
    private final Image bulletTexture;
    private final GameController gameController;
    private AsteroidComponent asteroidComponent;


    public BulletComponent(GameController gameController, Player player) {
        this.gameController = gameController;
        this.bulletList = new ArrayList<>();
        this.player = player;
        this.bulletTexture = new Image(Config.BULLET_IMAGE_PATH);
        this.asteroidComponent = getAsteroidComponent();
    }

    private AsteroidComponent getAsteroidComponent() {
        final ComponentContainer componentContainer = ComponentContainer.getInstance();
        return (AsteroidComponent) componentContainer.getComponent(AsteroidComponent.class.getSimpleName());
    }

    public List<Bullet> getBulletList() {
        return bulletList;
    }


    @Override
    public void update(double elapsedTime) {
        bulletList.forEach(bullet -> bullet.update(elapsedTime));
        bulletList.removeIf(bullet -> bullet.getPosition().y < 20);
    }

    @Override
    public void handleInput(List<String> inputs) {

    }

    @Override
    public void handleCollision() {
        if (asteroidComponent != null) {

            bulletList.forEach(bullet -> {
                final boolean hit = asteroidComponent.getAsteroidList().removeIf(target -> target.intersects(bullet));
                if (hit) {
                    asteroidComponent.createAsteroids(1);
                    player.score();
                }
            });
        } else {
            asteroidComponent = (getAsteroidComponent());
        }
    }

    @Override
    public void render(GraphicsContext graphicsContext) {
        bulletList.forEach(bullet -> bullet.render(graphicsContext));
    }

    protected void addBullet(final Ship ship) {
        Bullet bullet = new Bullet(bulletTexture);
        bullet.setPosition((ship.getPosition().x + ship.getCenter().x), ship.getPosition().y - 10);
        bullet.setSize(bullet.getSize().x, bullet.getSize().y / 3);
        bulletList.add(bullet);
    }
}

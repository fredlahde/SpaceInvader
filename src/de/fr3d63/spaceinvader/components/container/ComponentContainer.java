package de.fr3d63.spaceinvader.components.container;

import de.fr3d63.spaceinvader.components.IComponent;

import java.util.HashMap;
import java.util.Map;

public class ComponentContainer {
    private static ComponentContainer ourInstance = new ComponentContainer();

    public static ComponentContainer getInstance() {
        return ourInstance;
    }

    private final Map<String , IComponent> iComponentMap;

    private ComponentContainer() {
        iComponentMap = new HashMap<>();
    }

    public void add(final IComponent ... components) {
        for (IComponent component : components) {
            this.iComponentMap.put(component.getClass().getSimpleName(), component);
        }
    }

    public void forEach(final IComponentAction actionInterface) {
        iComponentMap.values().forEach(actionInterface::action);
    }

    public IComponent getComponent(String simpleName) {
        return iComponentMap.get(simpleName);
    }
}


package de.fr3d63.spaceinvader.components.container;

import de.fr3d63.spaceinvader.components.IComponent;

@FunctionalInterface
public interface IComponentAction {
    void action(IComponent component);
}

package de.fr3d63.spaceinvader.components;

import com.sun.javafx.geom.Vec2d;
import de.fr3d63.spaceinvader.components.container.ComponentContainer;
import de.fr3d63.spaceinvader.config.Config;
import de.fr3d63.spaceinvader.controller.GameController;
import de.fr3d63.spaceinvader.player.Player;
import de.fr3d63.spaceinvader.sprites.Ship;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.List;

public class ShipComponent implements IComponent {

    private final Ship ship;
    private final GameController gameController;
    private final Player player;

    public ShipComponent(GameController gameController, Player player) {
        this.gameController = gameController;
        this.player = player;

        ship = new Ship();
        ship.setImage(Config.SHIP_IMAGE_PATH);
        final double scale = .205;
        ship.setSize(ship.getSize().x * scale, ship.getSize().y * scale);
        ship.setPosition(new Vec2d(0, Config.GAMEOVER_BOUND));
    }

    public Ship getShip() {
        return ship;
    }

    @Override
    public void update(double elapsedTime) {
        ship.update(elapsedTime);

        final int shipMaxX = ((int) (Config.WINDOW_MAX_X - ship.getSize().x));
        if (ship.getPosition().x >= shipMaxX) {
            ship.setPosition(shipMaxX, ship.getPosition().y);
        }

        if (ship.getPosition().x <= 0) {
            ship.setPosition(0, ship.getPosition().y);
        }
    }

    @Override
    public void handleInput(List<String> inputs) {
        final int boost = 1600;

        ship.setVelocity(0, 0);

        if (!gameController.isGameOver()) {
            if (inputs.contains("LEFT")) {
                ship.addVelocity(-boost, 0);
            }

            if (inputs.contains("RIGHT")) {
                ship.addVelocity(boost, 0);
            }

            if (inputs.contains("SPACE")) {
                inputs.remove("SPACE");
                final ComponentContainer componentContainer = ComponentContainer.getInstance();
                ((BulletComponent) componentContainer.getComponent(BulletComponent.class.getSimpleName())).addBullet(ship);
            }
        }
    }

    @Override
    public void handleCollision() {
        final ComponentContainer componentContainer = ComponentContainer.getInstance();
        final AsteroidComponent asteroidComponent
                = ((AsteroidComponent) componentContainer.getComponent(AsteroidComponent.class.getSimpleName()));

        if (asteroidComponent != null) {
            asteroidComponent.getAsteroidList().forEach(target -> {
                if (target.intersects(ship)) {
                    gameController.setGameOver(true);
                    player.setCanScore(false);
                }
            });
        }
    }

    @Override
    public void render(GraphicsContext graphicsContext) {
        ship.render(graphicsContext);
        if (Config.DRAW_SPRITE_BOUNDARY) {
            drawBoundaries(graphicsContext, ship.getBoundary(), Color.RED);
        }
    }


}

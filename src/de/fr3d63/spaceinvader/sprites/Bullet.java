package de.fr3d63.spaceinvader.sprites;

import com.sun.javafx.geom.Vec2d;
import javafx.scene.image.Image;

/**
 * Created by fr3d63 on 22/09/16.
 */
public class Bullet extends Sprite2d {

    public Bullet(String path) {
        super(path);
        this.setVelocity(new Vec2d(0,- 100));
    }

    public Bullet(Image bulletTexture) {
        super(bulletTexture);
    }

    @Override
    public void update(double time) {
        this.getPosition().y -= 10;
    }

    @Override
    public Vec2d getCenter() {
        return new Vec2d(getSize().x / 2, getSize().y / 2);

    }
}

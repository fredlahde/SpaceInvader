package de.fr3d63.spaceinvader.sprites;

import com.sun.javafx.geom.Vec2d;
import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;
import javafx.geometry.Rectangle2D;

public abstract class Sprite2d {
    private Image image;
    private Vec2d position;
    private Vec2d velocity;
    private Vec2d size;

    public Sprite2d() {
        position = new Vec2d(0, 0);
        velocity = new Vec2d(0, 0);
    }

    public Sprite2d(Image image) {
        this();
        this.setImage(image);
    }

    public Sprite2d(String path) {
        this(new Image(path));
    }

    public void setImage(Image i) {
        image = i;
        size = new Vec2d(i.getWidth(), i.getHeight());
    }

    public Vec2d getPosition() {
        return position;
    }

    public void setPosition(Vec2d position) {
        this.position = position;
    }

    public void setPosition(final double x, final double y) {
        setPosition(new Vec2d(x,y));
    }

    public Vec2d getVelocity() {
        return velocity;
    }

    public void setVelocity(Vec2d velocity) {
        this.velocity = velocity;
    }

    public void setVelocity(final double x, final double y) {
        setVelocity(new Vec2d(x,y));
    }

    public Vec2d getSize() {
        return size;
    }

    public void setSize(Vec2d size) {
        this.size = size;
    }

    public void setSize(final double width, final double height) {
        setSize(new Vec2d(width,height));
    }

    public void setImage(String filename) {
        Image i = new Image(filename);
        setImage(i);
    }

    public void addVelocity(double x, double y) {
        this.velocity = new Vec2d(velocity.x + x, velocity.y + y);
    }

    public void update(double time) {
        position = new Vec2d(
                position.x + (velocity.x * time),
                position.y + (velocity.y * time)
        );

    }

    public void render(GraphicsContext gc) {
        gc.drawImage(image, position.x, position.y, size.x, size.y);
    }

    public Rectangle2D getBoundary() {
        return new Rectangle2D(position.x, position.y, size.x, size.y);
    }

    public boolean intersects(Sprite2d s) {
        return s.getBoundary().intersects(this.getBoundary());
    }

    public abstract Vec2d getCenter();

}
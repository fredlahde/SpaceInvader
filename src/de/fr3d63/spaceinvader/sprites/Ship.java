package de.fr3d63.spaceinvader.sprites;

import com.sun.javafx.geom.Vec2d;
import javafx.geometry.Rectangle2D;

public class Ship extends Sprite2d {

    @Override
    public Vec2d getCenter() {
        return new Vec2d(getSize().x / 2, getSize().y / 2);
    }

}

package de.fr3d63.spaceinvader.sprites;

import com.sun.javafx.geom.Vec2d;
import javafx.scene.image.Image;

public class Asteroid extends Sprite2d {

    public Asteroid(String path) {
        super(path);
    }

    public Asteroid(Image targetTexture) {

        super(targetTexture);
    }

    @Override
    public Vec2d getCenter() {
        return new Vec2d(getSize().x / 2, getSize().y / 2);

    }
}

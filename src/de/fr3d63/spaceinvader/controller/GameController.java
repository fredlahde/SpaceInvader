package de.fr3d63.spaceinvader.controller;

import de.fr3d63.spaceinvader.components.*;
import de.fr3d63.spaceinvader.components.container.ComponentContainer;
import de.fr3d63.spaceinvader.components.IComponent;
import de.fr3d63.spaceinvader.player.Player;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.List;


public class GameController extends AnimationTimer {

    private final GraphicsContext graphicsContext;
    private final Canvas canvas;
    private final List<String> input;
    private final ComponentContainer componentContainer = ComponentContainer.getInstance();
    private double lastNanoTime = 0;


    private boolean gameOver = false;
    private boolean wasReset = false;
    private double fps;

    public GameController(final Canvas canvas, final List<String> input, Player player) {
        this.canvas = canvas;
        this.input = input;
        this.graphicsContext = this.canvas.getGraphicsContext2D();


        final BulletComponent bulletComponent = new BulletComponent(this, player);
        final AsteroidComponent asteroidComponent = new AsteroidComponent(this, player);
        final ShipComponent shipComponent = new ShipComponent(this, player);
        final HudComponent hudComponent = new HudComponent(player, this);
        final ResetComponent resetComponent = new ResetComponent(this, player);

        componentContainer.add
                (
                        bulletComponent,
                        asteroidComponent,
                        shipComponent,
                        hudComponent,
                        resetComponent
                );

    }

    @Override
    public void handle(final long currentNanoTime) {

        if (wasReset) {
            graphicsContext.setFill(Color.BLACK);
            graphicsContext.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
            wasReset = false;
        }

        double elapsedTime = (currentNanoTime - lastNanoTime) / 1000000000.0;
        fps = 1000000000.0 / (currentNanoTime - lastNanoTime);
        lastNanoTime = currentNanoTime;

        componentContainer.forEach(component -> component.handleInput(input));
        componentContainer.forEach(IComponent::handleCollision);
        componentContainer.forEach(component -> component.update(elapsedTime));

        graphicsContext.setFill(Color.BLACK);
        graphicsContext.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        componentContainer.forEach(component -> component.render(graphicsContext));


    }

    @Override
    public void stop() {
        this.gameOver = true;
        super.stop();
    }

    @Override
    public void start() {
        this.gameOver = false;
        super.start();
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public double getFps() {
        return fps;
    }
}

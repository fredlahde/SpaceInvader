package de.fr3d63.spaceinvader;

import de.fr3d63.spaceinvader.config.Config;
import de.fr3d63.spaceinvader.player.Player;
import de.fr3d63.spaceinvader.controller.GameController;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    @Override
    public void start(final Stage primaryStage) throws Exception {
        primaryStage.setTitle("Space Invaders");
        primaryStage.setResizable(false);
        final Group root = new Group();
        final Scene mainScene = new Scene(root);
        primaryStage.setScene(mainScene);

        final Canvas canvas = new Canvas(Config.WINDOW_MAX_X, Config.WINDOW_MAX_Y);
        root.getChildren().add(canvas);
        final List<String> input = new ArrayList<>();

        mainScene.setOnKeyPressed(e -> {
            String code = e.getCode().toString();
            if (!input.contains(code)) {
                input.add(code);
            }
        });

        mainScene.setOnKeyReleased(e -> {
            String code = e.getCode().toString();
            input.remove(code);
        });

        final Player player = new Player();
        final GameController gameController = new GameController(canvas, input, player);
        gameController.start();


        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

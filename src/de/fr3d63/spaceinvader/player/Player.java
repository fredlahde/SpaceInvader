package de.fr3d63.spaceinvader.player;

public class Player {

    private int score;
    private boolean canScore = true;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void score() {
         if (canScore) {
            this.score++;
        }
    }

    public boolean isCanScore() {
        return canScore;
    }

    public void setCanScore(boolean canScore) {
        this.canScore = canScore;
    }
}

package de.fr3d63.spaceinvader.config;

public final class Config {

    public static final int WINDOW_MAX_X = 1024;
    public static final int WINDOW_MAX_Y = 1024;

    public static final int TARGET_MAX_X = 900;
    public static final int TARGET_MIN_X = 100;
    public static final int TARGET_MAX_Y = 400;

    public static final int GAMEOVER_BOUND = 750;

    public final static String BULLET_IMAGE_PATH = "bullet.png";
    public final static String SHIP_IMAGE_PATH = "ship.png";
    public final static String TARGET_IMAGE_PATH = "asteroid.png";


    public static final boolean DRAW_SPRITE_BOUNDARY = false;
}
